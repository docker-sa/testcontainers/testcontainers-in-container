#!/bin/bash
set -o allexport; source settings.env; set +o allexport

docker run -it --name go-runner \
--privileged \
--env TESTCONTAINERS_HOST_OVERRIDE=host.docker.internal \
--env DOCKER_HOST=unix:///var/run/docker.sock \
--env TESTCONTAINERS_RYUK_DISABLED=true \
--env TESTCONTAINERS_DOCKER_SOCKET_OVERRIDE=/var/run/docker-alt.sock \
--volume /var/run/docker.sock:/var/run/docker.sock \
-v $PWD:$PWD -w $PWD \
--rm k33g/go-runner:${VERSION} 

# --env TESTCONTAINERS_HOST_OVERRIDE=host.docker.internal \
# --env TESTCONTAINERS_HOST_OVERRIDE=docker.svc.local \
# --mount type=bind,source="$(pwd)"/project,target=/project \
# sudo chmod 666 /var/run/docker.sock

