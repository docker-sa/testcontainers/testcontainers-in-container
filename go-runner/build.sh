#!/bin/bash
set -o allexport; source settings.env; set +o allexport

docker buildx build \
--platform=linux/arm64 \
--build-arg="GO_VERSION=${GO_VERSION}" \
--build-arg="USER_NAME=${USER_NAME}" \
--load \
-t k33g/go-runner:${VERSION} .

docker images | grep go-runner
