package main

import (
	"context"
	"log"
	"testing"

	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/require"
	"github.com/testcontainers/testcontainers-go"
	redisModule "github.com/testcontainers/testcontainers-go/modules/redis"
)

func TestIncrement(t *testing.T) {
	ctx := context.Background()

	redisContainer, err := redisModule.RunContainer(ctx, testcontainers.WithImage("redis:7.2.4"))
	require.NoError(t, err)

	log.Println("👋 Hello World 🌍")

	endpoint, err := redisContainer.Endpoint(ctx, "")
	require.NoError(t, err)

	//redisContainer.

	host, _ := redisContainer.Host(ctx)
	port, _ := redisContainer.MappedPort(ctx, "6379")

	log.Println("🤖", endpoint, host, port)

	rdb := redis.NewClient(&redis.Options{
		Addr: endpoint,
	})

	_, err = rdb.Set(ctx, counterKey, "19", 0).Result()
	require.NoError(t, err)

	v := increment(ctx, rdb)
	require.Equal(t, int64(20), v)
}
