# testcontainers-in-container
🚧 It's a work in progress

What Ì'm triying to achieve is to run a testcontainers test inside a container. 

In the `go-runner` directory, I have a Dockerfile that builds a container with the necessary dependencies to run a go test.

Once the image is built (see `go-runner/build.sh`), I run the container with the following command:

```bash
cd go-runner

export VERSION="experiments" 
docker run -it --name go-runner \
--privileged \
--env TESTCONTAINERS_HOST_OVERRIDE=host.docker.internal \
--env DOCKER_HOST=unix:///var/run/docker.sock \
--env TESTCONTAINERS_RYUK_DISABLED=true \
--env TESTCONTAINERS_DOCKER_SOCKET_OVERRIDE=/var/run/docker-alt.sock \
--volume /var/run/docker.sock:/var/run/docker.sock \
-v $PWD:$PWD -w $PWD \
--rm k33g/go-runner:${VERSION} 
```

When I'm inside the container, I run the test with the following command:

```bash
cd webapp
go test
```

The TestContainers is able to start a Redis container:
```golang
redisContainer, err := redisModule.RunContainer(ctx, testcontainers.WithImage("redis:7.2.4"))
```

```bash
2024/06/04 08:32:48 github.com/testcontainers/testcontainers-go - Connected to docker: 
  Server Version: 26.1.1
  API Version: 1.44
  Operating System: Docker Desktop
  Total Memory: 7840 MB
  Resolved Docker Host: unix:///var/run/docker.sock
  Resolved Docker Socket Path: /var/run/docker-alt.sock
  Test SessionID: 2f4a1d288f2c8ba202260e665f42b889ca10c6f0fa95bbd613f45e45a5f7af8a
  Test ProcessID: 719a649f-1e0f-4b06-a66d-7d3b2c5926ac
2024/06/04 08:32:48 🐳 Creating container for image redis:7.2.4
2024/06/04 08:32:49 ✅ Container created: fcaaa9a76af3
2024/06/04 08:32:49 🐳 Starting container: fcaaa9a76af3
2024/06/04 08:32:49 ✅ Container started: fcaaa9a76af3
2024/06/04 08:32:49 🚧 Waiting for container id fcaaa9a76af3 image: redis:7.2.4. Waiting for: &{timeout:<nil> Log:* Ready to accept connections IsRegexp:false Occurrence:1 PollInterval:100ms}
2024/06/04 08:32:49 🔔 Container is ready: fcaaa9a76af3
2024/06/04 08:32:49 👋 Hello World 🌍
2024/06/04 08:32:49 🤖 172.17.0.1:54051 172.17.0.1 54051/tcp
```

But it fails when it tries to connect to the Redis db:
```bash
    main_test.go:37: 
                Error Trace:    /Users/k33g/Library/CloudStorage/Dropbox/docker-sa/TestContainers/testcontainers-in-container/go-runner/webapp/main_test.go:37
                Error:          Received unexpected error:
                                dial tcp 172.17.0.1:54051: connect: connection refused
                Test:           TestIncrement
--- FAIL: TestIncrement (0.38s)
FAIL
exit status 1
FAIL    web-app 0.382s
```
