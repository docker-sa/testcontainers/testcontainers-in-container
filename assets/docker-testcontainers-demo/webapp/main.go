// Package main : a simple web app
package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/redis/go-redis/v9"
)

/*
GetBytesBody returns the body of an HTTP request as a []byte.
  - It takes a pointer to an http.Request as a parameter.
  - It returns a []byte.

func GetBytesBody(request *http.Request) []byte {
	body := make([]byte, request.ContentLength)
	request.Body.Read(body)
	return body
}
*/

const counterKey = "counter"

func main() {

	ctx := context.Background()

	var redisHost = os.Getenv("REDIS_HOST")
	if redisHost == "" {
		redisHost = "redis:6379"
	}

	log.Println("🚀 connecting to redis at: " + redisHost)

	// Redis
	rdb := redis.NewClient(&redis.Options{
		Addr:     redisHost,
	})

	var httpPort = os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}

	var message = os.Getenv("MESSAGE")
	if message == "" {
		message = "this is a message"
	}

	log.Println("🚀 starting web server on port: " + httpPort)
	log.Println("📝 message: " + message)

	mux := http.NewServeMux()

	fileServerHtml := http.FileServer(http.Dir("public"))
	mux.Handle("/", fileServerHtml)

	mux.HandleFunc("/counter", func(response http.ResponseWriter, request *http.Request) {
		c := increment(ctx, rdb)
		log.Println("🧮", c)
		response.WriteHeader(http.StatusOK)
		response.Write([]byte(strconv.FormatInt(c, 10)))
	})

	mux.HandleFunc("/info", func(response http.ResponseWriter, request *http.Request) {
		// read the content of the file info.txt
		// and return it as a response
		content, err := os.ReadFile("./public/info.txt")
		if err != nil {
			response.WriteHeader(http.StatusNoContent)
		}
		response.WriteHeader(http.StatusOK)
		response.Write(content)
	})

	mux.HandleFunc("/variables", func(response http.ResponseWriter, request *http.Request) {

		variables := map[string]interface{}{
			"message": message,
		}

		jsonString, err := json.Marshal(variables)

		if err != nil {
			response.WriteHeader(http.StatusNoContent)
		}

		response.Header().Set("Content-Type", "application/json; charset=utf-8")
		response.WriteHeader(http.StatusOK)
		response.Write(jsonString)
	})

	var errListening error
	log.Println("🌍 http server is listening on: " + httpPort)
	errListening = http.ListenAndServe(":"+httpPort, mux)

	log.Fatal(errListening)
}

func increment(ctx context.Context, rdb *redis.Client) int64 {
	c, err := rdb.Incr(ctx, counterKey).Result()

	if err != nil {
		log.Println("😡", err)
		return 0
	}
	return c
}
